﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeCamps.Web.Models
{
    public class CodeCamp
    {
        public int CodeCampID { get; set; }

        public string Name { get; set; }

        public DateTime EventDate { get; set; }

        public string EventDateString { get { return EventDate.ToShortDateString(); } }

        public string City { get; set; }

        public string State { get; set; }

        public string Country { get; set; }

        public double DistanceMiles { get; set; }

        public string DistanceMilesString { get { return DistanceMiles.ToString("0.00"); } }
    }
}