﻿using CodeCamps.Web.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeCamps.Web.Controllers
{
    public class CodeCampsApiController : ApiController
    {
        public IEnumerable<CodeCamp> Get(string state)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                conn.Open();

                return conn.Query<CodeCamp>(
                    sql: "SELECT * FROM CodeCamps WHERE State = @State",
                    param: new { State = state }
                    );
            }
        }

        public IEnumerable<CodeCamp> Get(double latitude, double longitude)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                conn.Open();

                return conn.Query<CodeCamp>(
                    sql: "GetCodeCampsByPosition",
                    param: new { Latitude = latitude, Longitude = longitude },
                    commandType: CommandType.StoredProcedure
                    );
            }
        }
    }
}
