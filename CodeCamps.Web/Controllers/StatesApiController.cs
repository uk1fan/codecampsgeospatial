﻿using CodeCamps.Web.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CodeCamps.Web.Controllers
{
    public class StatesApiController : ApiController
    {
        public State Get(double latitude, double longitude)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                conn.Open();

                return conn.Query<State>(
                    sql: "GetStateFromPosition", 
                    param: new { Latitude = latitude, Longitude = longitude }, 
                    commandType: CommandType.StoredProcedure
                    ).FirstOrDefault();
            }
        }
    }
}
