﻿using CodeCamps.Metro.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeCamps.Metro.DataModel
{
    public class CodeCamp : BindableBase
    {
        private string _name = string.Empty;
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }

        private string _eventDate = string.Empty;
        public string EventDate
        {
            get { return this._eventDate; }
            set { this.SetProperty(ref this._eventDate, value); }
        }

        private string _city = string.Empty;
        public string City
        {
            get { return this._city; }
            set { this.SetProperty(ref this._city, value); }
        }

        private string _state = string.Empty;
        public string State
        {
            get { return this._state; }
            set { this.SetProperty(ref this._state, value); }
        }

        private string _country = string.Empty;
        public string Country
        {
            get { return this._country; }
            set { this.SetProperty(ref this._country, value); }
        }

        private double _distanceMiles;
        public double DistanceMiles
        {
            get { return this._distanceMiles; }
            set { this.SetProperty(ref this._distanceMiles, value); }
        }

        private double _latitude;
        public double Latitude
        {
            get { return this._latitude; }
            set { this.SetProperty(ref this._latitude, value); }
        }

        private double _longitude;
        public double Longitude
        {
            get { return this._longitude; }
            set { this.SetProperty(ref this._longitude, value); }
        }

        public string Title
        {
            get { return Name; }
        }

        public string Subtitle
        {
            get { return City + ", " + State + " " + Country + " - " + DistanceMiles.ToString("0.00") + " miles away"; }
        }
    }
}
